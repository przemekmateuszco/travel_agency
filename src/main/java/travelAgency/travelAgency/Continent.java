package travelAgency.travelAgency;

import javax.persistence.*;

@Entity(name = "continent")
public class Continent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "name")
    private String name;

    public Continent() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}


