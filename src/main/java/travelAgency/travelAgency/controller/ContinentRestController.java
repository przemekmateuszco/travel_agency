package travelAgency.travelAgency.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travelAgency.travelAgency.Continent;
import travelAgency.travelAgency.service.ContinentServiceSingleton;

import java.util.List;

@RestController
@RequestMapping("/continents")
public class ContinentRestController {

    private final ContinentServiceSingleton continentServiceSingleton;

    public ContinentRestController(ContinentServiceSingleton continentServiceSingleton) {
        this.continentServiceSingleton = continentServiceSingleton;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<Continent> saveContinentFromRestController(@RequestBody Continent continent) {
        Continent continentAdded = continentServiceSingleton.addContinent(continent);
        return ResponseEntity.ok(continentAdded);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/show_all")
    public ResponseEntity<List<Continent>> showContinents() {
        List<Continent> allContinents = continentServiceSingleton.showAllContinents();
        return ResponseEntity.ok(allContinents);
    }
}
