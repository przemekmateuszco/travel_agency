package travelAgency.travelAgency.service;

import org.springframework.beans.factory.annotation.Autowired;
import travelAgency.travelAgency.Trip;
import travelAgency.travelAgency.repository.TripRepo;

import java.util.List;

public class TripServiceSingleton implements TripService {

    @Autowired
    private TripRepo tripRepo;

    @Override
    public Trip addTrip(Trip trip) {
        return tripRepo.save(trip);
    }

    @Override
    public Trip getTripById(Long id) {
        return tripRepo.findById(id).get();
    }

    @Override
    public Trip modifyTrip(Trip trip) {
        return tripRepo.save(trip);
    }

    @Override
    public boolean deleteTrip(Trip trip) {
        try {
            tripRepo.delete(trip);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public List<Trip> getAllTrips() {
        return tripRepo.findAll();
    }
}
