package travelAgency.travelAgency.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import travelAgency.travelAgency.Continent;
import travelAgency.travelAgency.repository.ContinentRepo;

import java.util.List;

@Service
public class ContinentServiceSingleton implements ContinentService {

    @Autowired
    private ContinentRepo continentRepo;

    @Override
    public Continent addContinent(Continent continent) {
        return continentRepo.save(continent);
    }

    @Override
    public List<Continent> showAllContinents() {
        return continentRepo.findAll();
    }
}
