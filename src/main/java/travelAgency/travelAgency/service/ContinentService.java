package travelAgency.travelAgency.service;

import travelAgency.travelAgency.Continent;

import java.util.List;

public interface ContinentService {
    Continent addContinent(Continent continent);
    List<Continent> showAllContinents();
}
