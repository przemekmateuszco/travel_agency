package travelAgency.travelAgency.service;

import travelAgency.travelAgency.Trip;

import java.util.List;

public interface TripService {
    Trip addTrip(Trip trip);
    Trip getTripById(Long id);
    Trip modifyTrip(Trip trip);
    boolean deleteTrip(Trip trip);

    List<Trip> getAllTrips();
//    boolean isPromotion(Long id);
//    List<Trip> getTripsByAirport(Airport airport);
//    List<Trip> getTripsByHotel(Hotel hotel);
}
