package travelAgency.travelAgency;

import javax.persistence.*;

@Entity(name = "country")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "continent_id")
    private Continent continentId;

    public Country(String name, Continent continentId) {
        this.name = name;
        this.continentId = continentId;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Continent getContinentId() {
        return continentId;
    }
}


