package travelAgency.travelAgency;

import javax.persistence.*;

@Entity(name = "purchasedTrip")
public class PurchasedTrip {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "trip_id")
    private Trip tripId;


    @Column(name = "amount")
    private Long amount;
    @Column(name = "owner")
    private String owner;
}
