package travelAgency.travelAgency;

import javax.persistence.*;

@Entity(name = "hotel")
public class Hotel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "standard")
    private int standard;
    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "cityId")
    private City cityId;

    public Hotel(String name, int standard, String description, City cityId) {
        this.name = name;
        this.standard = standard;
        this.description = description;
        this.cityId = cityId;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getStandard() {
        return standard;
    }

    public String getDescription() {
        return description;
    }

    public City getCityId() {
        return cityId;
    }
}
