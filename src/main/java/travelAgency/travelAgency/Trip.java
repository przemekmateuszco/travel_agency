package travelAgency.travelAgency;

import travelAgency.travelAgency.model.FoodType;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "trip")
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "trip_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "airport_from")
    private Airport airportFrom;

    @ManyToOne
    @JoinColumn(name = "airport_to")
    private Airport airportTo;

    @ManyToOne
    @JoinColumn(name = "hotel")
    private Hotel hotelTo;

    @ManyToOne
    @JoinColumn(name = "city")
    private City cityTo;

    @Temporal(value = TemporalType.DATE)
//    @Column(name = "departure")
    private Date departureDate;
    @Temporal(value = TemporalType.DATE)
    private Date returnDate;

    @Column(name = "days_count")
    private int countOfDays;

    @Enumerated(value = EnumType.STRING)
    private FoodType type;

    @Column(name = "price_adult")
    private double priceForAdult;
    @Column(name = "price_child")
    private double priceForChild;
    @Column(name = "promotion")
    private boolean promotion;
    @Column(name = "person_count")
    private int countOfPersons;
    @Column(name = "description")
    private String description;
}
