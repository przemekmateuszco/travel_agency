package travelAgency.travelAgency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import travelAgency.travelAgency.Trip;

@Repository
public interface TripRepo extends JpaRepository<Trip, Long> {
}
