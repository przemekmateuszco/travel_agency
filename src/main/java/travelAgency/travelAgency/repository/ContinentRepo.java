package travelAgency.travelAgency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import travelAgency.travelAgency.Continent;

@Repository
public interface ContinentRepo extends JpaRepository<Continent, Long> {
}
