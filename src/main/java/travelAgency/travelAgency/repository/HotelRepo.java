package travelAgency.travelAgency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import travelAgency.travelAgency.Hotel;

@Repository
public interface HotelRepo extends JpaRepository<Hotel,Long> {
}
