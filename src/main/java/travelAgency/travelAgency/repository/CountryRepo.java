package travelAgency.travelAgency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import travelAgency.travelAgency.Country;

@Repository
public interface CountryRepo extends JpaRepository<Country, Long> {

}
