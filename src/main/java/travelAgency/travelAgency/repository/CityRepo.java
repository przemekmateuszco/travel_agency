package travelAgency.travelAgency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import travelAgency.travelAgency.City;

@Repository
public interface CityRepo extends JpaRepository<City, Long> {
}
